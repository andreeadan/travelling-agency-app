package com.example.calatour2.view_holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.calatour2.R
import com.example.calatour2.model.ChatMessage

class ChatMessageViewHolder(itemView: View, design: Int) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private var usernameTextView: TextView
    private var contentTextView: TextView
    private var timestampTextView: TextView
    private var starImageView: ImageView
    private lateinit var dataCache: ChatMessage

    init {
        if (design == R.layout.list_element_chat1) {
            usernameTextView = itemView.findViewById(R.id.chatMessageUsernameTextView)
            contentTextView = itemView.findViewById(R.id.chatMessageContentTextView)
            timestampTextView = itemView.findViewById(R.id.chatMessageTimestampTextView)
            starImageView = itemView.findViewById(R.id.chatMessageStarImageView)

        } else {
            usernameTextView = itemView.findViewById(R.id.chatMessageUsername2TextView)
            contentTextView = itemView.findViewById(R.id.chatMessageContent2TextView)
            timestampTextView = itemView.findViewById(R.id.chatMessageTimestamp2TextView)
            starImageView = itemView.findViewById(R.id.chatMessageStar2ImageView)
        }
        itemView.setOnClickListener(this)
    }

    fun bindData(data: ChatMessage){
        dataCache = data
        usernameTextView.text = data.username
        contentTextView.text = data.message
        timestampTextView.text = data.timestamp
        if(data.isImportant){
            starImageView.visibility = View.VISIBLE
        }else{
            starImageView.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        dataCache.isImportant = ! dataCache.isImportant
        if(dataCache.isImportant){
            starImageView.visibility = View.VISIBLE
        }else{
            starImageView.visibility = View.GONE
        }
    }
}