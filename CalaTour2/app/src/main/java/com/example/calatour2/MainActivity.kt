package com.example.calatour2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.calatour.model.chat_api.ErrorDetails
import com.example.calatour2.model.chat_api.AuthenticateRequest
import com.example.calatour2.model.chat_api.AuthenticateResponse
import com.example.calatour2.model.rest_api.ChatAPI
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Response
import kotlin.coroutines.EmptyCoroutineContext

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val chatAPI = ChatAPI.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val signInButton = findViewById<Button>(R.id.signInButton)
        signInButton.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        val usernameField = findViewById<EditText>(R.id.usernameEditText)
        val usernameError = findViewById<TextView>(R.id.usernameErrorTextView)
        val globalError = findViewById<TextView>(R.id.globalErrorTextView)

        val passwordField = findViewById<EditText>(R.id.passwordEditText)
        val passwordError = findViewById<TextView>(R.id.passwordErrorTextView)

        if(usernameField.text.toString().isEmpty()){
            usernameError.text = getString(R.string.usernameErrorEmpty)
        }else if(usernameField.text.toString().length <3){
            usernameError.text = getString(R.string.usernameErrorShort)
        }else{
            usernameError.text = ""
        }

        if(passwordField.text.toString().isEmpty()){
            passwordError.text = getString(R.string.passwordErrorEmpty)
        }else if(passwordField.text.toString().length < 6){
            passwordError.text = getString(R.string.passwordErrorShort)
        }else{
            passwordError.text =""
        }

        /*if(usernameField.text.toString().equals("admin") && passwordField.text.toString().equals("password")){
            /*globalError.text = getString(R.string.correctLogin)
            globalError.setTextColor(getResources().getColor(R.color.customGreen))*/
            val intent = Intent(this, ListOfOffersActivity::class.java)
            startActivity(intent)
        }else{
            globalError.text = getString(R.string.wrongLogin)
            globalError.setTextColor(getResources().getColor(R.color.customRed))
        }*/

        chatAPI.authenticate(AuthenticateRequest(usernameField.text.toString(), passwordField.text.toString())).enqueue(object : retrofit2.Callback<AuthenticateResponse>{
            override fun onResponse(
                call: Call<AuthenticateResponse>,
                response: Response<AuthenticateResponse>
            ) {
                if(response.isSuccessful()){
                    val intent = Intent(this@MainActivity, ListOfOffersActivity::class.java)
                    startActivity(intent)
                }else{
                    var errorDetails = ErrorDetails("Server did not send error details!")
                    try{
                        if(response.errorBody()!=null){
                            var rawErrorDetails = response.errorBody()?.string()
                            var parser = Gson()
                            errorDetails = parser.fromJson(rawErrorDetails, ErrorDetails::class.java)
                        }
                    }catch(e: Exception){
                        errorDetails = ErrorDetails("Error accessing the details from the server's error!")
                    }

                    globalError.text = errorDetails.message
                    globalError.setTextColor(getResources().getColor(R.color.customRed))
                }
            }

            override fun onFailure(call: Call<AuthenticateResponse>, t: Throwable) {
                Toast.makeText(applicationContext, "Please verify the INTERNET connection!", Toast.LENGTH_LONG).show()
            }

        })

    }
}
