package com.example.calatour2.model

import java.io.Serializable

class Offer (

    val title: String,
    val description: String,
    val price: Int,
    val image: Int,
    var isFavorite: Boolean = false,
    var contorDetails: Int = 0

): Serializable