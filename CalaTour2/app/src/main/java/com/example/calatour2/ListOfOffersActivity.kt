package com.example.calatour2

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.ListView
import com.example.calatour2.OfferDetails
import com.example.calatour2.adapter.ListOfOffersAdapter
import com.example.calatour2.model.GenerateOffer
import com.example.calatour2.model.Offer

class ListOfOffersActivity : AppCompatActivity(), AdapterView.OnItemClickListener {

    private lateinit var listOfOffersAdapter: ListOfOffersAdapter
    private var offers_counter:Int =1;
    private var ID_ACTIVITY_DETAILS: Int = 1001;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_offers)

        listOfOffersAdapter = ListOfOffersAdapter(this, GenerateOffer().execute())
        var offersListView = findViewById<ListView>(R.id.listOffersListView)
        offersListView.adapter = listOfOffersAdapter

        registerForContextMenu(offersListView)

        offersListView.setOnItemClickListener(this)
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo:
    ContextMenu.ContextMenuInfo?)
    {
        super.onCreateContextMenu(menu, v, menuInfo)
        // check if the menu is created for the targeted list
        if (v!!.id == R.id.listOffersListView)
        {
            // identify the item selected from the list
            val info = menuInfo as AdapterView.AdapterContextMenuInfo
            val offer = listOfOffersAdapter.getItem(info.position) as Offer
            menu!!.setHeaderTitle(offer.title)
            // load the visual structure of the menu
            getMenuInflater().inflate(R.menu.offers_contextual_menu, menu);
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean
    {
        // accessing the information attached to the context meniu
        val menuInfo = item.menuInfo as AdapterView.AdapterContextMenuInfo
        // identify the selected menu item using the predefined IDs
        if(item.itemId == R.id.menuElementAddOffer)
        {
            var offer = Offer("Added offer "+offers_counter++, "Added offer description...", 400, R.drawable.offer_1)
            listOfOffersAdapter.addOffer(menuInfo.position, offer)

        }
        else if (item.itemId == R.id.menuElementRemoveOffer)
        {
            listOfOffersAdapter.removeOffer(menuInfo.position)
        }
        listOfOffersAdapter.notifyDataSetChanged()
        return super.onContextItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.offers_options_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menuElementSignOut){

            signOutRequest()

        }else if(item.itemId == R.id.menuElementResetList){

        }else if(item.itemId == R.id.menuElementClearFavorites){

        }else if(item.itemId == R.id.elementMenuChat){
            val intent = Intent(this, ChatActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    fun signOutRequest(){
        val builder = AlertDialog.Builder ( this )
        builder.setTitle ( " Confirmation" )
            .setMessage ( " Are you sure you want to sign out? " )
            .setPositiveButton ( " Yes ", DialogInterface.OnClickListener {
                    dialog, id ->
                finish()
            })
            .setNegativeButton ( " Cancel ", null )
        builder.create().show()
    }


    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val offer = listOfOffersAdapter.getItem(position) as Offer
        val intent = Intent(this, OfferDetails::class.java)
        intent.putExtra("offer_content", offer)
        intent.putExtra("offer_position", position)
        startActivityForResult(intent, ID_ACTIVITY_DETAILS)
    }

    override fun onBackPressed() {
        signOutRequest()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if( requestCode  == ID_ACTIVITY_DETAILS){
            if( resultCode == Activity.RESULT_OK){
                val offerIsFavorite = data!!.getBooleanExtra("favorite_state", false)
                val offerContor = data!!.getIntExtra("contor_state", -1)
                val position = data!!.getIntExtra("offer_position", -1)
                if(position > -1){
                    val Offerr = listOfOffersAdapter.getItem(position) as Offer
                    Offerr.isFavorite= offerIsFavorite
                    Offerr.contorDetails = offerContor
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}