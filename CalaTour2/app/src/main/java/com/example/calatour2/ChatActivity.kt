package com.example.calatour2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.calatour2.adapter.ListChatAdapter
import com.example.calatour2.model.ChatMessage
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class ChatActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var listChatAdapter: ListChatAdapter
    private var dataSource= ArrayList<ChatMessage>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        listChatAdapter = ListChatAdapter(this, dataSource)

        val layourManager = LinearLayoutManager(this)
        layourManager.orientation = LinearLayoutManager.VERTICAL

        ///mesaje hardcodate
        /*dataSource.add(ChatMessage("admin", "Mesah chat text 1", "2020-12-10 19:50"))
        dataSource.add(ChatMessage("admin", "Mesah chat text 2", "2020-12-10 19:51", true))*/

        var listChatRecyclerView = findViewById<RecyclerView>(R.id.chatMessagesRecyclerView)
        listChatRecyclerView.layoutManager = layourManager
        listChatRecyclerView.adapter = listChatAdapter

        var sendButton = findViewById<Button>(R.id.addMessageSendButton)
        sendButton.setOnClickListener(this)
    }

    override fun onClick(view: View?){
        var contentField = findViewById<EditText>(R.id.addMessageContentEditText)
        dataSource.add(0,ChatMessage("admin", contentField.text.toString(), DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now())))
        listChatAdapter.notifyItemInserted(0)
        contentField.text.clear()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.chat_menu_options, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.chatMenuOptionsDesign1 ->{
                listChatAdapter.setDesign(1)
            }

            R.id.chatMenuOptionsDesign2 ->{
                listChatAdapter.setDesign(2)
            }

            R.id.chatMenuOptionsBothDesigns ->{
                listChatAdapter.setDesign(3)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}