package com.example.calatour2.model

class ChatMessage (
    val username: String,
    val message: String,
    val timestamp: String,
    var isImportant: Boolean = false
)