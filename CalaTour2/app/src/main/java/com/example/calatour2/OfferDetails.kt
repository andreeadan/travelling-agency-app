package com.example.calatour2

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.calatour2.R
import com.example.calatour2.model.Offer

class OfferDetails : AppCompatActivity() {

    private lateinit var offer: Offer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offer_details)

        offer = intent.getSerializableExtra("offer_content") as Offer
        val titluTextView = findViewById<TextView>(R.id.detaliiOfertaTitluTextView)
        val pretTextView = findViewById<TextView>(R.id.detaliiOfertaPretTextView)
        val descriereTextView = findViewById<TextView>(R.id.detaliiOfertaDescriereTextView)
        val imagineTextView = findViewById<ImageView>(R.id.detaliiOfertaImagineImageView)

        titluTextView.text = offer.title
        pretTextView.text = offer.price.toString() + " EUR"
        descriereTextView.text = offer.description
        imagineTextView.setImageResource(offer.image)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.offers_details_options_menu, menu)
        val contorTextView = findViewById<TextView>(R.id.detaliiOfertaContorTextView)
        offer.contorDetails = offer.contorDetails +1
        contorTextView.text = "Number of views: "+offer.contorDetails.toString()
        if(offer.isFavorite){
            menu!!.getItem(0).title = getString(R.string.menuElementRemoveFavorites)
        }else{
            menu!!.getItem(0).title = getString(R.string.menuElementAddFavorites)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menuElementComuteFavorites){
            offer.isFavorite = !offer.isFavorite
            if(offer.isFavorite){
                item.title = getString(R.string.menuElementRemoveFavorites)
                val toast = Toast.makeText( applicationContext, "Offer was added to Favorites", Toast.LENGTH_SHORT).show()

                /*val toast = Toast.makeText(applicationContext, "Offer was added to Favorites", Toast.LENGTH_SHORT);
                val view = toast.getView();
                view!!.setBackgroundColor(getColor(R.color.black))*/

            }else{
                item.title = getString(R.string.menuElementAddFavorites)
                Toast.makeText ( applicationContext, "Offer was removed from Favorites", Toast.LENGTH_SHORT
                ).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("favorite_state", offer.isFavorite)
        intent.putExtra("contor_state", offer.contorDetails)
        intent.putExtra("offer_position", this.intent.getIntExtra("offer_position", -1))
        setResult(RESULT_OK, intent)
        finish()
    }
}