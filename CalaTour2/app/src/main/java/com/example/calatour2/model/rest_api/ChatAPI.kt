package com.example.calatour2.model.rest_api

import com.example.calatour2.model.chat_api.AuthenticateRequest
import com.example.calatour2.model.chat_api.AuthenticateResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ChatAPI {

    @POST("authenticate.php")
    @Headers( "Content-Type: application/json" )
    fun authenticate(@Body body: AuthenticateRequest): Call<AuthenticateResponse>

    companion object {
        private val httpInterceptor = HttpLoggingInterceptor().apply {
            // there are different logging levels that provide a various amount of detail
            // we will use the most detailed one
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        private val httpClient = OkHttpClient.Builder().apply {
            // add the interceptor to the newly created HTTP client
            this.addInterceptor(httpInterceptor)
        }.build()


        fun create(): ChatAPI {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://cgisdev.utcluj.ro/moodle/chat-piu/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build()
            return retrofit.create(ChatAPI::class.java)
        }
    }
}