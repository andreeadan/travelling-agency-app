package com.example.calatour2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.calatour2.R
import com.example.calatour2.model.ChatMessage
import com.example.calatour2.view_holders.ChatMessageViewHolder

class ListChatAdapter (
    private val context: Context,
    private val dataSource: ArrayList<ChatMessage>
): RecyclerView.Adapter<ChatMessageViewHolder>(){

    // which design to be used at a time
    private var chosenDesign = 1

    // get a reference to the LayoutInflater service
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatMessageViewHolder {
        var rowView = inflater.inflate(viewType, parent, false)
        return ChatMessageViewHolder(rowView, viewType)
    }

    override fun onBindViewHolder(holder: ChatMessageViewHolder, position: Int) {
        holder.bindData(dataSource.elementAt(position))
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun getItemViewType(position: Int):Int{
        when (chosenDesign){
            1-> return R.layout.list_element_chat1
            2-> return R.layout.list_element_chat2
            else->{
                if(position%2==0){
                    return R.layout.list_element_chat2
                }else{
                    return R.layout.list_element_chat1
                }
            }
        }
    }

    fun setDesign(design: Int){
        chosenDesign = design;

        // sa se reafiseze continutul, fiindca toate mesajele vor fi afectate de schimbare
        notifyDataSetChanged()
    }
}