package com.example.calatour2.model

import com.example.calatour2.R

class GenerateOffer {

    fun execute(): ArrayList<Offer>{
        val offers = ArrayList<Offer>()
        offers.add(
            Offer(
                "Barcelona, 3 nights",
                "Vacation at Barcelona",
                300,
                R.drawable.offer_1
            )
        )
        offers.add(
            Offer(
                "Maldive, 7 nights",
                "Vacation at Maldive",
                1050,
                R.drawable.offer_2
            )
        )
        offers.add(
            Offer(
                "Thailanda, 10 nights",
                "Vacation at Thailanda",
                1210,
                R.drawable.offer_3
            )
        )
        return offers
    }

}