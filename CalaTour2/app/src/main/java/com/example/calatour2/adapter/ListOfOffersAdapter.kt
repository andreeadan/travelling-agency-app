package com.example.calatour2.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.calatour2.ListOfOffersActivity
import com.example.calatour2.R
import com.example.calatour2.model.Offer

class ListOfOffersAdapter(
    private val context: Context,
    private val dataSource: ArrayList<Offer>
) : BaseAdapter() {

    // get a reference to the LayoutInflater service
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource.elementAt(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView (position: Int, containerView: View?, viewGroupParent: ViewGroup?): View
    {
        // get view for row item
        // check if we can reuse a previously defined cell which now is not visible anymore
        val rowView = inflater.inflate(R.layout.element_list_of_offers, viewGroupParent, false)

        // get the visual elements and update them with the information from the model
        val offerTitle = rowView.findViewById<TextView>(R.id.elementOfferTitleTextView)
        offerTitle.text = dataSource.elementAt(position).title

        val offerPrice = rowView.findViewById<TextView>(R.id.elementOfferPriceTextView)
        offerPrice.text = dataSource.elementAt(position).price.toString()

        val offerDescription = rowView.findViewById<TextView>(R.id.elementOfferDescriptionTextView)
        offerDescription.text = dataSource.elementAt(position).description

        val offerImage = rowView.findViewById<ImageView>(R.id.elementOfferImgImageView)
        offerImage.setImageResource(dataSource.elementAt(position).image)

        return rowView
    }

    fun removeOffer(position: Int){
        dataSource.removeAt(position)
    }

    fun addOffer(position: Int, offer: Offer){
        dataSource.add(position, offer)
    }

}