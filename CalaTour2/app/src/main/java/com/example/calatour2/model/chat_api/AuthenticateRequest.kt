package com.example.calatour2.model.chat_api

data class AuthenticateRequest (
    val username: String,
    val password: String
)