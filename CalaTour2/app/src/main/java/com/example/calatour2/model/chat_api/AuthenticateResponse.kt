package com.example.calatour2.model.chat_api

data class AuthenticateResponse (
    val id: String,
    val token: String,
    val display: String
)
